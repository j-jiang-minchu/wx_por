// #ifndef VUE3
import Vue from 'vue'
import App from './App'
// 导入 store 的实例对象
import store from '@/store/store.js'
// 引入字体图标
import '@/static/iconfont.css'
// 导入第三方网络请求的包
import { $http } from '@escook/request-miniprogram'
// $http挂载到uni上使用
uni.$http = $http
$http.baseUrl = 'https://api-ugo-web.itheima.net'
// 请求拦截器
// 请求开始之前做一些事情
$http.beforeRequest = function(options) {
	wx.showLoading({ title: '数据加载中...', })
	// 判断请求的是否为有权限的 API 接口
	if (options.url.indexOf('/my/') !== -1) {
		// 为请求头添加身份认证字段
		options.header = {
			// 字段的值可以直接从 vuex 中进行获取
			Authorization: store.state.m_user.token,
		}
	}
}
// 响应拦截器
// 请求完成之后做一些事情
$http.afterRequest = function() {
	wx.hideLoading()
}
// 封装的展示消息提示的方法
uni.showMsg = function(title = '加载数据失败', duration = 1500) {
	uni.showToast({
		title,
		duration,
		icon: 'none',
	})
}
Vue.config.productionTip = false

App.mpType = 'app'

const app = new Vue({
	...App,
	// 将 store 挂载到 Vue 实例上
	store
})
app.$mount()
// #endif

// #ifdef VUE3
import { createSSRApp } from 'vue'
export function createApp() {
	const app = createSSRApp(App)
	return { app }
}
// #endif
